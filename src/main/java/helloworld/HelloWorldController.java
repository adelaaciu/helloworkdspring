package helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/helloWorld")
public class HelloWorldController {

	@GetMapping(value = "/getMessage")
	public String getHelloMessage() {
		return "Hello world";
	}
}
